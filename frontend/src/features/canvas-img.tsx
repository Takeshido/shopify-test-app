import { getFitImgSettings } from '@/utils/canvas';
import { FC, useEffect, useRef } from 'react';

type Props = {
  src: string;
} & React.CanvasHTMLAttributes<HTMLCanvasElement>;

export const CanvasImg: FC<Props> = ({ src, ...props }) => {
  const canvasRef = useRef<HTMLCanvasElement>(null);

  useEffect(() => {
    const canvas = canvasRef.current!;
    const ctx = canvas.getContext('2d')!;

    const img = new Image();
    img.src = src;
    img.onload = () => {
      ctx.clearRect(0, 0, canvas.width, canvas.height);
      ctx.drawImage(img, ...getFitImgSettings(canvas, img));
    };
  }, []);

  return <canvas ref={canvasRef} {...props}></canvas>;
};
