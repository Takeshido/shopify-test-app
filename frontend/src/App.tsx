import { useQuery } from '@apollo/client';
import { Product } from '@/components/product';
import type { TProduct } from '@/types/product';
import { GET_PRODUCTS } from '@/graphql/product';

export const App = () => {
  const { data, error, loading } = useQuery<{ products: TProduct[] }>(GET_PRODUCTS);

  if (loading) return 'Loading...';
  if (error) return `Error ${error}`;
  return (
    <div className="bg-white">
      <div className="mx-auto w-11/12 py-6 md:py-12">
        <div className="grid grid-cols-1 gap-x-6 gap-y-10 sm:grid-cols-2 lg:grid-cols-4 xl:gap-x-8">
          {data?.products.map((product) => <Product key={product.id} product={product} />)}
        </div>
      </div>
    </div>
  );
};
