import { FC } from 'react';
import type { TProduct } from '@/types/product';
import { imgUrlRegExp } from '@/constants';
import { CanvasImg } from '@/features/canvas-img';

type Props = {
  product: TProduct;
};

export const Product: FC<Props> = ({ product }) => {
  const bodyHtmlWithoutImgUrls = product.bodyHtml.replace(imgUrlRegExp, '');

  return (
    <div className="group relative">
      <div className="flex items-center justify-center rounded-md py-4 bg-slate-100 group-hover:opacity-90">
        <CanvasImg src={product.images[0]} height={250} />
      </div>
      <div
        className="text-sm p-2 border-x border-b border-slate-200"
        dangerouslySetInnerHTML={{ __html: bodyHtmlWithoutImgUrls.split('base_price')[0] }}
      />
    </div>
  );
};
