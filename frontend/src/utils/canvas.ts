type Settings = [number, number, number, number, number, number, number, number];

export const getFitImgSettings = (canvas: HTMLCanvasElement, img: HTMLImageElement): Settings => {
  const hRatio = canvas.width / img.width;
  const wRatio = canvas.height / img.height;
  const ratio = Math.min(hRatio, wRatio);
  const centerX = (canvas.width - img.width * ratio) / 2;
  const centerY = (canvas.height - img.height * ratio) / 2;

  return [0, 0, img.width, img.height, centerX, centerY, img.width * ratio, img.height * ratio];
};
