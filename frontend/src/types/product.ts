export type TProduct = {
  id: string;
  bodyHtml: string;
  images: string[];
};
