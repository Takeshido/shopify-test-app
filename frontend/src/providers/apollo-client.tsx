import { ApolloClient, InMemoryCache, ApolloProvider } from '@apollo/client';
import { FC, PropsWithChildren } from 'react';

const client = new ApolloClient({
  uri: import.meta.env.VITE_BASE_URI,
  headers: {
    'Content-type': 'application/json; charset=utf-8',
  },
  cache: new InMemoryCache(),
});

export const ApolloClientProvider: FC<PropsWithChildren> = ({ children }) => {
  return <ApolloProvider client={client}>{children}</ApolloProvider>;
};
