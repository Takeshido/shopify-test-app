import React from 'react';
import ReactDOM from 'react-dom/client';
import { App } from '@/App';
import { ApolloClientProvider } from '@/providers/apollo-client';
import './index.css';

ReactDOM.createRoot(document.getElementById('root')!).render(
  <React.StrictMode>
    <ApolloClientProvider>
      <App />
    </ApolloClientProvider>
  </React.StrictMode>
);
