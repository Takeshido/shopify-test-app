import { createAdminApiClient } from '@shopify/admin-api-client';
import { prisma } from '@src/index';

const client = createAdminApiClient({
  storeDomain: 'cpb-new-developer.myshopify.com',
  apiVersion: '2023-10',
  accessToken: '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',
});

const baseProductsQuery = `{
  products(first: 5) {
    edges {
      node {
        id
        bodyHtml
        images(first: 3) {
          nodes {
            src
          }
        }
      }
    }
  }
}`;

type TData = {
  products: {
    edges: [
      {
        node: {
          id: string;
          bodyHtml: string;
          images: {
            nodes: [{ src: string }];
          };
        };
      }
    ];
  };
};

export const saveProductsFromShopify = async () => {
  const { data, errors } = await client.request<TData>(baseProductsQuery);
  if (data) {
    const baseProducts = data.products.edges.map(({ node }) => ({
      ...node,
      images: [...node.images.nodes.map(({ src }) => src)],
    }));

    const { count } = await prisma.product.createMany({
      data: baseProducts,
      skipDuplicates: true,
    });

    console.log(`Saved products count: ${count}`);
  } else {
    console.log(errors);
  }
};
