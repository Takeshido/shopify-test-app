import { prisma } from '@src/index';

export const getProducts = async () => {
  return await prisma.product.findMany();
};
