import { GraphQLResolveInfo } from 'graphql';
import { getProducts } from '../services/product.service';

export const productQueryResolver = {
  async products(_: any, args: Record<string, any>, context: any, info: GraphQLResolveInfo) {
    return await getProducts();
  },
};
