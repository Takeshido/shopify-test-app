import { readFileSync } from 'fs';
import path from 'path';
import { productQueryResolver } from './resolvers/product.resolver';

const productTypes = readFileSync(path.join(__dirname, './typeDefs/product.graphql'), {
  encoding: 'utf-8',
});

export const typeDefs = `
    ${productTypes}
`;

export const resolvers = {
  Query: { ...productQueryResolver },
};
