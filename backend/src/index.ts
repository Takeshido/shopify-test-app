import express, { urlencoded } from 'express';
import dotenv from 'dotenv';
import cors from 'cors';
import { ApolloServer } from '@apollo/server';
import { expressMiddleware } from '@apollo/server/express4';
import { resolvers, typeDefs } from './graphql';
import { PrismaClient } from '@prisma/client';
import { saveProductsFromShopify } from '@src/graphql/baseProductsQuery';

dotenv.config();

export const prisma = new PrismaClient();

const port = process.env.PORT || 3000;
const app = express();

const presetServer = async () => {
  const apolloServer = new ApolloServer({ typeDefs, resolvers });

  await apolloServer.start().then(() => {
    console.log(`Graphql started at http://localhost:${port}/graphql`);
  });

  app.use(cors());
  app.use(express.json());
  app.use(urlencoded({ extended: true }));
  app.use('/graphql', expressMiddleware(apolloServer));
};

presetServer().then(() => {
  app.listen(port, () => {
    console.log(`Express started at http://localhost:${port}`);
    saveProductsFromShopify();
  });
});
