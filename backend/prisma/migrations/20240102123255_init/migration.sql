-- CreateTable
CREATE TABLE "Product" (
    "id" STRING NOT NULL,
    "bodyHtml" STRING NOT NULL,
    "images" STRING[],

    CONSTRAINT "Product_pkey" PRIMARY KEY ("id")
);
